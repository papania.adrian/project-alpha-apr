"""tracker URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect


def redirect_to_projects_list(request):
    return redirect("list_projects")


def redirect_to_login_page(request):
    return redirect("login")


def redirect_to_signup_page(request):
    return redirect("signup")


def redirect_to_details_page(request, id):
    return redirect("show_project")


urlpatterns = [
    path("admin/", admin.site.urls),
    path("projects/", include("projects.urls")),
    path("", redirect_to_projects_list, name="home"),
    path("login/", redirect_to_login_page, name="login"),
    path("signup/", redirect_to_signup_page, name="signup"),
    path("accounts/", include("accounts.urls")),
    path("accounts/", include("django.contrib.auth.urls")),
    path("tasks/", include("tasks.urls")),
]
