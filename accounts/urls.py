from django.urls import path, include
from accounts.views import user_signup, user_login, user_logout


urlpatterns = [
    path("login/", user_login, name="login"),
    path("signup/", user_signup, name="signup"),
    path("accounts/", include("django.contrib.auth.urls")),
    path("logout/", user_logout, name="logout"),
]
